<?php
namespace EmailManager\Models;

use EmailManager\Lib\Database;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Models\DomainModel;

class DomainAdminModel {
    public const FIELDS = [
        'name' => 'string',
        'username' => 'string',
        'password' => 'string',
        'enabled' => 'boolean'
    ];

    public $id, $name, $username, $password, $enabled, $create_time, $update_time;

    public static function getAll () {
        $query = <<<EOD
                        SELECT name, username, password, enabled, create_time, update_time
                        FROM domain_admin
                    EOD;

        return Database::fetchAllObj($query, self::class);
    }

    public static function getAllForDomain ($domain) {
        $query = <<<EOD
                        SELECT domain_admin.id,
                               domain_admin.name,
                               domain_admin.username,
                               domain_admin.password,
                               domain_admin.enabled,
                               domain_admin.create_time,
                               domain_admin.update_time
                        FROM domain_admin, email_domain_has_domain_admin, email_domain
                        WHERE email_domain_has_domain_admin.domain_admin_id = domain_admin.id
                          AND email_domain_has_domain_admin.email_domain_id = email_domain.id
                          AND email_domain.name = :domain
                    EOD;

        return Database::fetchAllObj($query, self::class, ['domain' => $domain]);
    }

    public static function getAdmin ($username) {
        $query = <<<EOD
                        SELECT domain_admin.id,
                                domain_admin.name,
                                domain_admin.username,
                                domain_admin.password,
                                domain_admin.enabled,
                                domain_admin.create_time,
                                domain_admin.update_time
                        FROM domain_admin
                        WHERE name = :username
                    EOD;

        return Database::fetchSingleObj($query, self::class, ['username' => $username]);
    }

    public static function get_missing_admins ($admin_usernames) {
        $admin_placeholders = str_repeat('?, ', count($admin_usernames) - 1) . '?';
        $check_query = <<<EOD
                              SELECT username
                              FROM domain_admin
                              WHERE username IN ($admin_placeholders)
                          EOD;

        $valid_admins = array_map(function ($admin) {
            return $admin['username'];
        }, Database::fetchAll($check_query, $admin_usernames));

        $result = [];
        foreach ($admin_usernames as $admin_username) {
            if (!in_array($admin_username, $valid_admins)) {
                array_push($result, $admin_username);
            }
        }

        return $result;
    }

    public static function check_existing ($admin_usernames) {
        $missing_admins = DomainAdminModel::get_missing_admins($admin_usernames);
        return count($missing_admins) === 0;
    }
}
