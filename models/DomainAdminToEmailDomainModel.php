<?php
namespace EmailManager\Models;

use EmailManager\Lib\Database;
use EmailManager\Lib\ErrorHandler;

class DomainAdminToEmailDomainModel {
    public $email_domain_id;
    public $domain_admin_id;

    /**
     * @param string $domain
     * @param string[] $admin_usernames
     */
    public static function set_admins_for_domain ($domain, $admin_usernames) {
        $difference = self::get_admin_difference($domain, $admin_usernames);
        DomainAdminToEmailDomainModel::delete_admins_for_domain($domain, $difference[1]);
        DomainAdminToEmailDomainModel::add_admins_for_domain($domain, $difference[0]);
    }

    public static function get_admin_difference ($domain, $admin_usernames) {
        $domain_admins_query = <<<EOD
            SELECT domain_admin.username
            FROM email_domain_has_domain_admin, domain_admin
            WHERE email_domain_has_domain_admin.email_domain_id = (SELECT id FROM email_domain WHERE name = :domain)
              AND email_domain_has_domain_admin.domain_admin_id = domain_admin.id
        EOD;

        $existing_domain_admins = array_map(function ($admin) {
            return $admin['username'];
        }, Database::fetchAll($domain_admins_query, ['domain' => $domain]));

        $missing_admins = [];
        $additional_admins = [];
        foreach ($admin_usernames as $admin_username) {
            if (!in_array($admin_username, $existing_domain_admins)) {
                array_push($additional_admins, $admin_username);
            }
        }
        foreach ($existing_domain_admins as $admin_username) {
            if (!in_array($admin_username, $admin_usernames)) {
                array_push($missing_admins, $admin_username);
            }
        }

        return [$missing_admins, $additional_admins];
    }

    public static function add_admins_for_domain ($domain, $admin_usernames) {
        $query = <<<EOD
            INSERT INTO email_domain_has_domain_admin (email_domain_id, domain_admin_id)
            VALUES 
        EOD;

        foreach ($admin_usernames as $i => $admin_username) {
            $query .= '((SELECT id FROM email_domain WHERE name=?), (SELECT id FROM domain_admin WHERE username=?))';

            if ($i !== array_key_last($admin_usernames)) {
                $query .= ', ';
            }
        }

        $params = array_reduce($admin_usernames, function ($params, $admin_username) use ($domain) {
            $result = $params;
            array_push($result, $domain, $admin_username);
            return $result;
        }, []);

        Database::set($query, $params);
    }

    public static function delete_admins_for_domain ($domain, $admin_usernames) {
        $admin_placeholders = str_repeat('?, ', count($admin_usernames) - 1) . '?';
        $query = <<<EOD
            DELETE FROM email_domain_has_domain_admin
            WHERE domain_admin_id IN (SELECT id FROM domain_admin WHERE username IN ($admin_placeholders))
              AND email_domain_id = (SELECT id FROM email_domain WHERE name = ?)
         EOD;

        $params = $admin_usernames;
        array_push($params, $domain);

        Database::set($query, $params);
    }
}
