<?php
namespace EmailManager\Lib;

use EmailManager\Controller\AccountController;
use EmailManager\Controller\DomainAdminController;
use EmailManager\Controller\DomainController;
use EmailManager\Lib\Config\AppConfig;
use Flight;

class Router {
    function __construct () {
//        header('Content-Type: application/json');

        define('RESULT_OK', json_encode([ 'message' => 'ok' ]));

        // Check for API key header
        Flight::route('*', function () {
            $headers = getallheaders();

            if (isset($headers['X-Api-Key']) && $headers['X-Api-Key'] === AppConfig::$config->general->api_key) {
                return true;
            }

            if (isset($_GET['apiKey']) && $_GET['apiKey'] === AppConfig::$config->general->api_key) {
                return true;
            }

            ErrorHandler::handle(401);
            return false;
        });

        // Setup routes
        Flight::route('GET /api/domains', [DomainController::class, 'getAll']);
        Flight::route('GET /api/domains/@domain', [DomainController::class, 'getSingle']);
        Flight::route('GET /api/domains/@domain/admins', [DomainAdminController::class, 'getAll']);
        Flight::route('PUT /api/domains/@domain/admins', [DomainAdminController::class, 'update']);
        Flight::route('GET /api/domains/@domain/accounts', [DomainAdminController::class, 'getAll']);
        Flight::route('POST /api/domains/@domain/accounts', [DomainAdminController::class, 'add']);
        Flight::route('GET /api/domains/@domain', [DomainController::class, 'getSingle']);
        Flight::route('POST /api/domains', [DomainController::class, 'create']);
        Flight::route('PUT /api/domains/@domain', [DomainController::class, 'update']);
        Flight::route('DELETE /api/domains/@domain', [DomainController::class, 'delete']);
        Flight::route('PUT /api/domains/@domain', [DomainController::class, 'createAccount']);

        Flight::route('GET /api/accounts', [AccountController::class, 'getAll']);
        Flight::route('GET /api/accounts/@email', [AccountController::class, 'getSingle']);
        Flight::route('DELETE /api/accounts/@email', [AccountController::class, 'delete']);
        Flight::route('PUT /api/accounts/@email/aliases', [AccountController::class, 'addAlias']);
        Flight::route('DELETE /api/accounts/@email/aliases/@alias', [AccountController::class, 'deleteAlias']);
        Flight::route('PUT /api/accounts/@email/forwards', [AccountController::class, 'addForward']);
        Flight::route('DELETE /api/accounts/@email/forwards/@forward', [AccountController::class, 'deleteForward']);

        // Respond with 404 to every other request
        Flight::route('*', function () {
            ErrorHandler::handle(404);
        });
    }

    public function start () {
        Flight::start();
    }
}
