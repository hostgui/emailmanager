<?php
namespace EmailManager\Lib;

use EmailManager\Models\AliasModel;
use EmailManager\Models\ForwardModel;

class Helpers {
    private static function getBaseURL () {
        $scheme = $_SERVER['REQUEST_SCHEME'];
        $hostname = $_SERVER['SERVER_NAME'];
        $port = $_SERVER['SERVER_PORT'];
        $standardport = ($scheme === 'http' && $port === '80') || ($scheme === 'https' && $port === '443');

        return "$scheme://$hostname" . ($standardport ? '' : ":$port") . '/api';
    }

    public static function assembleURL ($endpoint) {
        $base = self::getBaseURL();
        $get = '';

        if (isset($_GET['apiKey'])) {
            $get .= '?apiKey=' . $_GET['apiKey'];
        }

        return $base . $endpoint . $get;
    }

    public static function parseEmail ($email) {
        $splt = explode('@', $email);

        if (count($splt) !== 2) {
            ErrorHandler::handle(400, 'Invalid email');
        }

        return [
            'username' => $splt[0],
            'domain' => $splt[1]
        ];
    }

    public static function assembleAccount ($account) {
        $email = "$account->username@$account->domain_name";

        return [
            'id' => $account->id,
            'username' => $account->username,
            'email' => $email,
            'aliases' => AliasModel::getAll($account->domain_name, $account->username),
            'forwards' => ForwardModel::getAll($account->domain_name, $account->username),
            'access_mail' => $account->access_mail,
            'receive_mail' => $account->receive_mail,
            'created_at' => $account->created_at,
            'updated_at' => $account->updated_at,
            'href' => Helpers::assembleURL("/accounts/$email")
        ];
    }
}
