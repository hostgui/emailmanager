<?php
namespace EmailManager\Lib;

use EmailManager\Lib\Config\AppConfig;
use EmailManager\Lib\Config\HookConfig;

class Hooks {
    public const
        ACCOUNT = 'account',
        ALIAS = 'alias',
        DOMAIN = 'domain',
        FORWARD = 'forward';

    public const
        CREATE = 'create',
        UPDATE = 'update',
        DELETE = 'delete';

    public const
        PRE = 'pre',
        POST = 'post';

    public const MODELS = [ self::ACCOUNT, self::ALIAS, self::DOMAIN, self::FORWARD ];
    public const CUD = [ self::CREATE, self::UPDATE, self::DELETE ];
    public const ACTIONS = [ self::PRE, self::POST ];

    private static $hooks = [];

    public static function init () {
        $hooks = AppConfig::$config->hooks;

        // Generate empty hook array
        foreach (self::MODELS as $model) {
            self::$hooks[$model] = [
                self::PRE => [
                    self::CREATE => [],
                    self::UPDATE => [],
                    self::DELETE => []
                ],
                self::POST => [
                    self::CREATE => [],
                    self::UPDATE => [],
                    self::DELETE => []
                ]
            ];
        }

        /** @var HookConfig $hook */
        foreach ($hooks as $hook)
            self::$hooks[$hook->model][$hook->action][$hook->cud] = [
                'execute' => $hook->execute,
                'required' => $hook->required
            ];
    }

    /**
     * Runs hook if it exists and calls ErrorHandle::handle if the executed command exits with a code other than 0 if required == true.
     * @param $model string
     * @param $action string
     * @param $crud string
     * @param $params array
     */
    public static function run ($model, $action, $crud, $params) {
        $hook = self::$hooks[$model][$action][$crud] ?? null;

        if ($hook != null) {
            $status = 0;

            $p = [];
            foreach ($params as $key => $value) {
                $p['{{'.$key.'}}'] = $value;
            }

            $p['{{model}}'] = $model;
            $p['{{action}}'] = $action;
            $p['{{crud}}'] = $crud;

            exec(strtr($hook['execute'], $p), $output, $status);

            if ($hook['required'] && $status != 0)
                ErrorHandler::handle(500, 'HookError');
        }
    }

    public static function runPreCreate ($model, $params) {
        self::run($model, self::PRE, self::CREATE, $params);
    }

    public static function runPostCreate ($model, $params) {
        self::run($model, self::POST, self::CREATE, $params);
    }


    public static function runPreUpdate ($model, $params) {
        self::run($model, self::PRE, self::UPDATE, $params);
    }

    public static function runPostUpdate ($model, $params) {
        self::run($model, self::POST, self::UPDATE, $params);
    }


    public static function runPreDelete ($model, $params) {
        self::run($model, self::PRE, self::DELETE, $params);
    }

    public static function runPostDelete ($model, $params) {
        self::run($model, self::POST, self::DELETE, $params);
    }
}