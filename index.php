<?php
namespace EmailManager;

require 'vendor/autoload.php';

use EmailManager\Lib\Config\AppConfig;
use EmailManager\Lib\Database;
use EmailManager\Lib\Hooks;
use EmailManager\Lib\Router;

AppConfig::init([
    '/etc/hosting/emailmanager.toml',
    '/etc/emailmanager.toml',
    __DIR__ . '/config.toml'
]);

Database::init();
Hooks::init();

$router = new Router();
$router->start();
