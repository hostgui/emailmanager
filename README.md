# EmailManager

## Set up

- `git clone https://gitlab.com/hostgui/emailmanager`
- `composer install`
- Set up Apache
- Enable `mod_rewrite`

## Using nginx or other webservers

`.htaccess` only works for the Apache webserver. For setting EmailManager up with another webserver, use http://flightphp.com/install/#configure-your-webserver for a reference.