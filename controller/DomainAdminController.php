<?php
namespace EmailManager\Controller;

use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Helpers;
use EmailManager\Models\DomainAdminModel;
use EmailManager\Models\DomainAdminToEmailDomainModel;
use Flight;

class DomainAdminController {
    public static function getAll ($domain) {
        $domain_admins = DomainAdminModel::getAllForDomain($domain);
        $result = array_map(function ($admin) {
            return [
                'username' => $admin->username,
                'href' => Helpers::assembleURL("/admins/$admin->username")
            ];
        }, $domain_admins);
        echo json_encode($result);
    }

    public static function update ($domain) {
        $data = Flight::request()->data;

        if (!isset($data['admins']) || gettype($data['admins']) !== 'array') {
            ErrorHandler::handle(400, "'admins' must be set and an array");
        }

        $missing_admins = DomainAdminModel::get_missing_admins($data['admins']);
        if (count($missing_admins) > 0) {
            ErrorHandler::handle(400, 'Invalid admins: ' . join(', ', $missing_admins));
        }

        DomainAdminToEmailDomainModel::set_admins_for_domain($domain, $data['admins']);
    }
}
