<?php
namespace EmailManager\Controller;

use EmailManager\Lib\Config\AppConfig;
use EmailManager\Models\AccountModel;
use EmailManager\Models\DomainAdminToEmailDomainModel;
use EmailManager\Models\DomainModel;
use EmailManager\Models\DomainAdminModel;
use EmailManager\Models\AliasModel;
use EmailManager\Models\ForwardModel;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Helpers;
use Flight;

class DomainController {
    public static function getAll () {
        $response = array_map(function ($domain) {
            return assembleResponseForDomain($domain);
        }, DomainModel::getAll());

        echo json_encode($response);
    }

    public static function getSingle ($domain_name) {
        $domain = DomainModel::getSingle($domain_name);
        $response = assembleResponseForDomain($domain, true);
        echo json_encode($response);
    }

    public static function create () {
        $data = Flight::request()->data;

        foreach (DomainModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $missing_admins = DomainAdminModel::get_missing_admins($data['admins']);
        if (count($missing_admins) > 0) {
            ErrorHandler::handle(400, 'Invalid admins: ' . join(', ', $missing_admins));
        }

        $domain = new DomainModel();
        $domain->name = $data['name'];
        $domain->quota = $data['quota'];
        $domain->add();

        if (isset($data['admins'])) {
            DomainAdminToEmailDomainModel::add_admins_for_domain($domain->name, $data['admins']);
        }

        echo RESULT_OK;
    }

    public static function update ($domain_name) {
        $data = Flight::request()->data;

        foreach (DomainModel::FIELDS as $key => $value) {
            if (isset($data[$key]) && gettype($data[$key]) != $value) {
                ErrorHandler::handle(400, "'$key' needs to be of type $value");
            }
        }

        foreach ($data as $key => $value) {
            if ($key !== 'quota') {
                ErrorHandler::handle(400, 'Only quota can be changed');
            }
        }

        $domain = DomainModel::getSingle($domain_name);

        if ($domain == null) {
            ErrorHandler::handle(404);
        }

        if (isset($data['quota'])) {
            $domain->quota = $data['quota'];
            $domain->update($domain_name);
        }

        echo RESULT_OK;
    }

    public static function delete ($domain) {
        if (DomainModel::getSingle($domain) == null)
            ErrorHandler::handle(404);

        DomainModel::delete($domain);

        echo RESULT_OK;
    }

    public static function createAccount ($domain) {
        $data = Flight::request()->data;

        foreach (AccountModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $account = new AccountModel();
        $account->username = $data['username'];
        $account->access_mail = $data['access_mail'];
        $account->receive_mail = $data['receive_mail'];
        $account->quota = $data['quota'];
        $account->setPassword($data['password'], AppConfig::$config->general);

        $account->add($domain);

        echo RESULT_OK;
    }
}

function assembleResponseForDomain ($domain, $with_accounts = false) {
    $response = [
        'id' => $domain->id,
        'name' => $domain->name,
        'quota' => $domain->quota,
        'href' => Helpers::assembleURL("/domains/$domain->name")
    ];

    $response['admins'] = array_map(function ($admin) {
        return [
            'id' => $admin->id,
            'name' => $admin->name,
            'username' => $admin->username,
            'enabled' => $admin->enabled === 1,
            'create_time' => $admin->create_time,
            'update_time' => $admin->update_time,
            'href' => Helpers::assembleURL("/admins/$admin->id")
        ];
    }, DomainAdminModel::getAllForDomain($domain->name));

    if ($with_accounts) {
        $response['accounts'] = array_map(function ($account) {
            return Helpers::assembleAccount($account);
        }, AccountModel::getAllForDomain($domain->name));
    }

    return $response;
}
