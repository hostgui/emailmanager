<?php
namespace EmailManager\Controller;

use EmailManager\Lib\Helpers;
use EmailManager\Models\AccountModel;
use Flight;

class DomainAccountController {
    public static function getAll ($domain) {
        $domain_accounts = AccountModel::getAllForDomain($domain);
        $result = array_map(function ($domain_account) {
            return Helpers::assembleAccount($domain_account);
        }, $domain_accounts);
        echo json_encode($result);
    }

    public static function add ($domain) {
        $data = Flight::request()->data;
    }
}
